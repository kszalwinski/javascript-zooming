'use strict';
$(document).ready(function () {
    var map = $("#map");
    var map2 = $("#map2");
    var mapWidth = map.width();
    var mapHeight = map.height();
    var currentZoom = 1;
    var currentScale = 1;
    var zoomStep = 1;
    var maxZoom = 10;

    map.css({
        position: 'absolute',
        left: 0,
        top: 0
    });


    window.addEventListener('click', function (event) {
        event.preventDefault();
        if (event.target === map[0]) {

            if (currentZoom + zoomStep <= maxZoom) {
                currentZoom += zoomStep;
            }

            console.log('currentScale', currentScale);
            console.log('currentZoom', currentZoom);

            map.width(mapWidth * currentZoom);
            map.height(mapHeight * currentZoom);

            map.css({

                /*left: -( event.offsetX / currentScale - event.offsetX / ( currentScale * currentZoom) ),
                top: -( event.offsetY / currentScale - event.offsetY / ( currentScale * currentZoom) )*/

                left: (1 - currentZoom)/currentScale * (event.offsetX),
                 top: (1 - currentZoom)/currentScale * (event.offsetY)
            });

            currentScale = currentZoom;
        }
        return false;
    });

    function mouseMove(event) {
        event.preventDefault();
        if (event.target === map[0]) {

            /* console.log('image-coordinates',event.offsetX,event.offsetY);
             console.log('map-coordinates',map.position().left,map.position().top);
             console.log('window-coordinates',event.clientX, event.clientY);*/

            //console.log('difference', (event.offsetX / map.width()) - (event.offsetX + map.position().left) / (map.width() + map.position().left));
            //console.log('relative-position', event.offsetX / map.width());
            //zoomingPoint = {x: event.offsetX / map[0].width, y: event.offsetY / map[0].height};
            //console.log(event.offsetX,event.offsetY);
            //console.log('parent', event.pageX-$(event.target.parentNode).position().left,event.pageY-$(event.target.parentNode).position().top); // współrzędne względem rodzica
            //console.log(zoomingPoint);
            /*console.log('global 1', event.clientX + document.body.scrollLeft, event.clientY + document.body.scrollTop); // współrzędne globalne
             //console.log('global 2', event.pageX, event.pageY); // współrzędne globalne
             /!*console.log('local ', event.offsetX, event.offsetY); // współrzędne myszy na obrazie*!/
             console.log('window ', event.clientX, event.clientY); // współrzędne względem okna przeglądarki*/
        }
    }

    window.addEventListener('mousemove', mouseMove);

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
        var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
        return {width: srcWidth * ratio, height: srcHeight * ratio};
    }
});
