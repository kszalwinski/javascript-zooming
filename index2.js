var current = {x: 0, y: 0, zoom: 1},
    con = document.getElementById('container');

window.onclick = function (e) {
    var coef = e.shiftKey || e.ctrlKey ? 0.5 : 1.2,
        oz = current.zoom,
        nz = current.zoom * coef,
    /// offset of container
        ox = 300,
        oy = 42,
    /// mouse cords
        mx = e.clientX - ox,
        my = e.clientY - oy,
    /// calculate click at current zoom
        ix = (mx - current.x) / oz,
        iy = (my - current.y) / oz,
    /// calculate click at new zoom
        nx = ix * nz,
        ny = iy * nz,
    /// move to the difference
    /// make sure we take mouse pointer offset into account!
        cx = (ix + (mx - ix) - nx),
        cy = (iy + (my - iy) - ny)
        ;
    // update current
    current.zoom = nz;
    current.x = cx;
    current.y = cy;
    /// make sure we translate before scale!
    con.style.transform
        = 'translate(' + cx + 'px, ' + cy + 'px) '
        + 'scale(' + nz + ')'
    ;
};